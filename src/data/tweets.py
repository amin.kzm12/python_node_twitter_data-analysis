import mongoengine


class Tweets(mongoengine.DynamicDocument):
    id_str = mongoengine.StringField()
    user = mongoengine.ObjectIdField()
    meta = {
      'db_alias': 'core',
      'collection': 'tweets',
      'ordering': ['-created']
    }
