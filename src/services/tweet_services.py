from data.tweets import Tweets
from typing import Optional, List
import json

from pymongo import MongoClient


class TweetServices:
  def __init__(self):
    client = MongoClient('mongodb://localhost:27017/')
    with client:
      db = client.twitter

    self.db = db
    self.tweets = db['tweets']

  def getPostById(self):
    return self.tweets.find({})[0]
